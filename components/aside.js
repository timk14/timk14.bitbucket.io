class Aside extends HTMLElement {
    constructor() {
      super();
    }


  connectedCallback() {
    this.innerHTML = `
    <aside>
      <div class="profile-img-wrapper">
          <img src="img/profilepic.jpeg" alt="profile">
      </div>
      <h1 class="profile-name">Tim Konopacki</h1>
      <div class="text-center">
          <span class="badge badge-white badge-pill profile-designation">University of Dayton</span>
      </div>
      <nav class="social-links">
          
          <a href="https://twitter.com/Somewhere5pm" class="social-link" target="_blank" rel="noopener noreferrer"><i class="fab fa-twitter"></i></a>
          <a href="https://github.com/timk14" class="social-link" target="_blank" rel="noopener noreferrer"><i class="fab fa-github"></i></a>
          <a href="https://www.linkedin.com/in/tim-konopacki/" class="social-link" target="_blank" rel="noopener noreferrer"><i class="fab fa-linkedin"></i></a>
      </nav>
      <div class="widget">
          <h5 class="widget-title">personal information</h5>
          <div class="widget-content">
              <p>MAJOR : Computer Science</p>
              <p>MINOR : Business Administration</p>
              <p>CONCENTRATION : Cyber Defence</p>
              <p>PHONE : 847-732-4331</p>
              <p>EMAIL : konopackit1@udayton.edu</p>
              <p>Location : Arlington Heights, IL</p>
              <a href="files/KonopackiResumeUD4.docx" download="TimKonopacki_Resume.docx"
              <button class="btn btn-download-cv btn-primary rounded-pill"> <img src="assets/images/download.svg" alt="download"
                  class="btn-img">DOWNLOAD RESUME </button>
              </a>
          </div>
      </div>
      <div class="widget card">
          <div class="card-body">
              <div class="widget-content">
                  <h5 class="widget-title card-title">LANGUAGES</h5>
                  <p>English : Native</p>
                  <p>Java : Intermediate</p>
                  <p>Python : Intermediate</p>
                  <p>C / C++ : Beginner</p>
                  <p>Golang : Beginner</p>
                  <p>JavaScript : Beginner</p>
              </div>
          </div>
      </div>
      <div class="widget card">
          <div class="card-body">
              <div class="widget-content">
                  <h5 class="widget-title card-title">MY INTERESTS</h5>
                  <p>Documentaries</p>
                  <p>3D Printing</p>
                  <p>Aviation</p>
                  <p>Fishing</p>
              </div>
          </div>
      </div>
      <div class="widget card">
          <div class="card-body">
              <div class="widget-content">
                  <h5 class="widget-title card-title">Extracurriculars</h5>
                  <p>Trombone</p>
                  <p>Water Polo</p>
                  <p>Intramurals</p>
              </div>
          </div>
      </div>
</aside>
    `;
  }
}

customElements.define('aside-component', Aside);